package ru.vironit.pharmacy.controllers;

import org.springframework.data.domain.*;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.vironit.pharmacy.dto.*;
import ru.vironit.pharmacy.dto.assemblers.*;
import ru.vironit.pharmacy.entities.*;
import ru.vironit.pharmacy.security.JwtUser;
import ru.vironit.pharmacy.services.*;

import java.text.ParseException;
import java.util.Date;

@Secured({"ROLE_ADMIN", "ROLE_DIRECTOR"})
@RestController
@RequestMapping(value = "/orders")
public class OrderController {

    private final OrderService orderService;
    private final OrderItemService itemService;
    private final OrderAssembler assembler;
    private final ManagerService managerService;
    private final CustomerService customerService;

    public OrderController(OrderService orderService,
                           OrderItemService itemService,
                           OrderAssembler assembler,
                           ManagerService managerService, CustomerService customerService) {
        this.orderService = orderService;
        this.itemService = itemService;
        this.assembler = assembler;
        this.managerService = managerService;
        this.customerService = customerService;
    }

    @GetMapping
    public ResponseEntity<CollectionModel<OrderDto>> getAll(PagedResourcesAssembler<Order> pagedResourcesAssembler,
                                                            @RequestParam(required = false) String managerLogin,
                                                            @RequestParam(required = false) Status status,
                                                            @RequestParam(required = false) String user,
                                                            @RequestParam(required = false) String date,
                                                            Pageable pageable) throws ParseException {
        Page<Order> orders = orderService.findAll(managerLogin, user, status, date, pageable);
        for (Order order : orders) {
            order.setItems(itemService.findItemsByOrder(order.getOrderId()));
        }
        PagedModel<OrderDto> dtos = pagedResourcesAssembler.toModel(orders, assembler);
        return ResponseEntity.ok(dtos);
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<OrderDto> getOne(@PathVariable Long orderId) {
        Order order = orderService.findById(orderId);
        order.setItems(itemService.findItemsByOrder(order.getOrderId()));
        OrderDto dto = assembler.toModel(order);
        return ResponseEntity.ok(dto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<OrderDto> complete(Authentication authentication,
                                             @PathVariable Long id) {
        JwtUser user = (JwtUser) authentication.getPrincipal();
        Manager manager = managerService.findByLogin(user.getUsername());
        Order order = orderService.completeOrder(manager, id);
        order.setItems(itemService.findItemsByOrder(order.getOrderId()));
        OrderDto dto = assembler.toModel(order);
        return ResponseEntity.ok(dto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<OrderDto> cancel(Authentication authentication,
                                           @PathVariable Long id) {
        JwtUser user = (JwtUser) authentication.getPrincipal();
        Manager manager = managerService.findByLogin(user.getUsername());
        Order order = orderService.cancelOrder(manager, id);
        order.setItems(itemService.findItemsByOrder(id));
        return ResponseEntity.ok(assembler.toModel(order));
    }

}
