package ru.vironit.pharmacy.controllers;

import org.springframework.data.domain.*;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.*;
import org.springframework.http.*;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.vironit.pharmacy.dto.*;
import ru.vironit.pharmacy.dto.assemblers.*;
import ru.vironit.pharmacy.entities.*;
import ru.vironit.pharmacy.security.JwtUser;
import ru.vironit.pharmacy.services.*;

import javax.validation.Valid;
import java.util.ArrayList;

@Secured({"ROLE_USER"})
@RestController
@RequestMapping("/customer")
public class PersonalProfileController {

    private final CustomerService customerService;
    private final CustomerAssembler assembler;
    private final OrderService orderService;
    private final OrderAssembler orderAssembler;

    public PersonalProfileController(CustomerService customerService,
                                     CustomerAssembler assembler,
                                     OrderService orderService,
                                     OrderAssembler orderAssembler) {
        this.customerService = customerService;
        this.assembler = assembler;
        this.orderService = orderService;
        this.orderAssembler = orderAssembler;
    }

    @GetMapping
    public ResponseEntity<CustomerDto> getCustomer(Authentication authentication) {
        JwtUser principal = (JwtUser) authentication.getPrincipal();
        Customer customer = customerService.findCustomerByPhone(principal.getUsername());
        CustomerDto dto = assembler.toModel(authentication, customer);
        return ResponseEntity.ok(dto);
    }

    @PutMapping
    public ResponseEntity<CustomerDto> updateCustomer(@Valid @RequestBody CustomerDto newCustomer, Authentication authentication) {
        JwtUser principal = (JwtUser) authentication.getPrincipal();
        Customer customer = customerService.findCustomerByPhone(principal.getUsername());
        CustomerDto customerDTO = assembler.toModel(authentication, customerService.updateCustomer(customer.getId(), newCustomer));
        return ResponseEntity.ok(customerDTO);
    }

    @DeleteMapping
    public ResponseEntity deleteCustomer(Authentication authentication) {
        JwtUser principal = (JwtUser) authentication.getPrincipal();
        Customer customer = customerService.findCustomerByPhone(principal.getUsername());
        customerService.deleteCustomer(customer.getId());
        return ResponseEntity.ok().body("delete was successfully");
    }

    @GetMapping("/orders")
    public ResponseEntity<CollectionModel<OrderDto>> customerOrders(PagedResourcesAssembler<Order> pagedResourcesAssembler,
                                                                    Pageable pageable,
                                                                    Authentication authentication) {
        JwtUser principal = (JwtUser) authentication.getPrincipal();
        Customer customer = customerService.findCustomerByPhone(principal.getUsername());
        Page<Order> orderList = orderService.findByCustomer(customer.getId(), pageable);
        if (orderList == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        customer.setCustomerOrders(orderList.getContent());
        PagedModel<OrderDto> dtos = pagedResourcesAssembler.toModel(orderList, orderAssembler);
        return ResponseEntity.ok(dtos);
    }

    @PostMapping("/order")
    public ResponseEntity createOrder(Authentication authentication, @RequestBody ArrayList<OrderItemDto> items) {
        JwtUser principal = (JwtUser) authentication.getPrincipal();
        Customer customer = customerService.findCustomerByPhone(principal.getUsername());
        Order order = orderService.createNewOrder(customer, items);
        OrderDto dto = orderAssembler.toModel(order);
        return ResponseEntity.ok(dto);
    }
}
