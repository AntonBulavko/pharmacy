package ru.vironit.pharmacy.controllers;

import org.springframework.data.domain.*;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.vironit.pharmacy.dto.ManagerDto;
import ru.vironit.pharmacy.dto.assemblers.ManagerAssembler;
import ru.vironit.pharmacy.entities.Manager;
import ru.vironit.pharmacy.security.JwtUser;
import ru.vironit.pharmacy.services.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/managers")
public class ManagerController {

    private final ManagerService managerService;
    private final OrderService orderService;
    private final ManagerAssembler assembler;

    public ManagerController(ManagerService managerService,
                             OrderService orderService,
                             ManagerAssembler assembler) {
        this.managerService = managerService;
        this.orderService = orderService;
        this.assembler = assembler;
    }

    @Secured({"ROLE_DIRECTOR"})
    @GetMapping("")
    public ResponseEntity<CollectionModel<ManagerDto>> findAll(PagedResourcesAssembler<Manager> pagedResourcesAssembler,
                                                               @RequestParam(required = false) String orderId,
                                                               Pageable pageable) {
        Page<Manager> managers = managerService.findAll(orderId, pageable);
        PagedModel<ManagerDto> dtos = pagedResourcesAssembler.toModel(managers, assembler);
        return ResponseEntity.ok(dtos);
    }

    @Secured({"ROLE_DIRECTOR"})
    @GetMapping("/{id}")
    public ResponseEntity<ManagerDto> getManager(@PathVariable Long id) {
        Manager manager = managerService.findById(id);
        manager.setOrdersCompleted(orderService.findByManager(id));
        ManagerDto dto = assembler.toModel(manager);
        return ResponseEntity.ok(dto);
    }

    @Secured({"ROLE_DIRECTOR"})
    @PostMapping("")
    public ResponseEntity<ManagerDto> createManager(@Valid @RequestBody ManagerDto newManager) {
        return ResponseEntity.ok(assembler.toModel(managerService.createManager(newManager)));
    }

    @Secured({"ROLE_DIRECTOR"})
    @PutMapping("/{id}")
    public ResponseEntity<ManagerDto> updateManager(@PathVariable Long id,
                                                    @Valid @RequestBody ManagerDto newManager) {
        ManagerDto manager = assembler.toModel(managerService.updateManager(id, newManager));
        return ResponseEntity.ok(manager);
    }

    @Secured({"ROLE_DIRECTOR"})
    @DeleteMapping("/{id}")
    public ResponseEntity deleteManager(@PathVariable Long id) {
        managerService.deleteManager(id);
        return ResponseEntity.ok().body("delete was successfully");
    }

    @Secured({"ROLE_ADMIN"})
    @GetMapping("/manager")
    public ResponseEntity getManager(Authentication authentication) {
        JwtUser principal = (JwtUser) authentication.getPrincipal();
        Manager manager = managerService.findByLogin(principal.getUsername());
        ManagerDto dto = assembler.toModel(authentication, manager);
        return ResponseEntity.ok(dto);
    }

    @Secured({"ROLE_ADMIN"})
    @PutMapping("/manager")
    public ResponseEntity updateManager(Authentication authentication,
                                        @Valid @RequestBody ManagerDto newManager) {
        JwtUser principal = (JwtUser) authentication.getPrincipal();
        Manager manager = managerService.findByLogin(principal.getUsername());
        ManagerDto dto = assembler.toModel(authentication, managerService.updateManager(manager.getId(), newManager));
        return ResponseEntity.ok(dto);
    }

}