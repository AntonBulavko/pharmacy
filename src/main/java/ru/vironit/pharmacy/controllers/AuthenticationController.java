package ru.vironit.pharmacy.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.*;
import org.springframework.security.core.*;
import org.springframework.web.bind.annotation.*;
import ru.vironit.pharmacy.dto.AuthorizationDto;
import ru.vironit.pharmacy.dto.CustomerDto;
import ru.vironit.pharmacy.dto.assemblers.CustomerAssembler;
import ru.vironit.pharmacy.entities.*;
import ru.vironit.pharmacy.exception.CustomerException;
import ru.vironit.pharmacy.security.*;
import ru.vironit.pharmacy.services.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

@RestController
public class AuthenticationController {

    private final AuthenticationManager manager;
    private final JwtTokenProvider tokenProvider;
    private final CustomerService customerService;
    private final ManagerService managerService;

    public AuthenticationController(AuthenticationManager manager,
                                    JwtTokenProvider tokenProvider,
                                    CustomerService customerService,
                                    ManagerService managerService) {
        this.manager = manager;
        this.tokenProvider = tokenProvider;
        this.customerService = customerService;
        this.managerService = managerService;
    }

    @PostMapping("/signup")
    public ResponseEntity<?> signUp(@Valid @RequestBody CustomerDto customerDto) throws URISyntaxException {
        Customer customer = customerService.createCustomer(customerDto);
        Authentication authentication = manager.authenticate(new UsernamePasswordAuthenticationToken(customerDto.getPhone(), customerDto.getPassword()));
        CustomerDto dto = new CustomerAssembler().toModel(authentication, customer);
        return ResponseEntity.created(new URI(dto.getRequiredLink("customer info").getHref())).body(createToken(customer.getPhone(), customer.getRole()));
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody AuthorizationDto authorizationDto) {
        try {
            String login = authorizationDto.getLogin();
            try {
                manager.authenticate(new UsernamePasswordAuthenticationToken(login, authorizationDto.getPassword()));
                Customer customer = customerService.findCustomerByPhone(login);
                return ResponseEntity.ok(createToken(login, customer.getRole()));
            } catch (CustomerException e) {
                manager.authenticate(new UsernamePasswordAuthenticationToken(login, authorizationDto.getPassword()));
                Manager manager = managerService.findByLogin(login);
                return ResponseEntity.ok(createToken(login, manager.getRole()));
            }
        } catch (AuthenticationException e) {
            return ResponseEntity.status(400).body(new BadCredentialsException("Invalid username or password").getMessage());
        }
    }

    private Map createToken(String login, Role role) {
        String token = tokenProvider.createToken(login, role);
        Map<Object, Object> response = new HashMap<>();
        response.put("username", login);
        response.put("token", token);
        return response;
    }

}
