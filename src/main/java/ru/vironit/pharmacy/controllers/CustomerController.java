package ru.vironit.pharmacy.controllers;

import org.springframework.data.domain.*;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.*;
import org.springframework.http.*;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.vironit.pharmacy.dto.CustomerDto;
import ru.vironit.pharmacy.dto.assemblers.*;
import ru.vironit.pharmacy.entities.Customer;
import ru.vironit.pharmacy.services.*;

@Secured({"ROLE_ADMIN", "ROLE_DIRECTOR"})
@RestController
@RequestMapping(value = "/customers")
public class CustomerController {

    private final CustomerService customerService;
    private final CustomerAssembler assembler;
    private final OrderService orderService;
    private final OrderAssembler orderAssembler;

    public CustomerController(CustomerService customerService,
                              CustomerAssembler assembler,
                              OrderService orderService,
                              OrderAssembler orderAssembler) {
        this.customerService = customerService;
        this.assembler = assembler;
        this.orderService = orderService;
        this.orderAssembler = orderAssembler;
    }

    @GetMapping
    public ResponseEntity<CollectionModel<CustomerDto>> findAll(PagedResourcesAssembler<Customer> pagedResourcesAssembler,
                                                                Pageable pageable) {
        Page<Customer> customers = customerService.findAll(pageable);
        PagedModel<CustomerDto> dtos = pagedResourcesAssembler.toModel(customers, assembler);
        return ResponseEntity.ok(dtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerDto> getCustomer(@PathVariable Long id) {
        Customer customer = customerService.findCustomerById(id);
        CustomerDto dto = assembler.toModel(customer);
        return ResponseEntity.ok(dto);
    }

    @GetMapping("{customerId}/orders")
    public ResponseEntity<?> getAllByCustomer(@PathVariable Long customerId) {
        Customer customer = customerService.findCustomerById(customerId);
        Pageable sortOrders = PageRequest.of(0,10,Sort.by("dateCreated"));
        customer.setCustomerOrders(orderService.findByCustomer(customerId, sortOrders).getContent());
        if (customer.getCustomerOrders() != null) {
            return ResponseEntity.ok(orderAssembler.toCollectionModel(customer.getCustomerOrders()));
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body("no orders for this user");
    }

}
