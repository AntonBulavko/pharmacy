package ru.vironit.pharmacy.controllers;

import org.springframework.data.domain.*;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import ru.vironit.pharmacy.dto.assemblers.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.vironit.pharmacy.dto.MedicineDto;
import ru.vironit.pharmacy.entities.Medicine;
import ru.vironit.pharmacy.services.MedicineService;

import java.net.URI;
import javax.validation.Valid;
import java.net.URISyntaxException;

@RestController
@RequestMapping(value = "/medicines")
public class MedicineController {

    private final MedicineService medicineService;
    private final MedicineAssembler assembler;

    public MedicineController(MedicineService medicineService,
                              MedicineAssembler assembler) {
        this.medicineService = medicineService;
        this.assembler = assembler;
    }

    @GetMapping("")
    public ResponseEntity<?> findAll(PagedResourcesAssembler<Medicine> pagedResourcesAssembler,
                                     @RequestParam(required = false) String name,
                                     @RequestParam(required = false) String provider,
                                     @RequestParam(required = false) String description,
                                     Pageable pageable) {
        Page<Medicine> medicines = medicineService.findAll(name, provider, description, pageable);
        PagedModel<MedicineDto> dtos = pagedResourcesAssembler.toModel(medicines, assembler);
        return ResponseEntity.ok(dtos);
    }


    @Secured({"ROLE_ADMIN", "ROLE_DIRECTOR"})
    @PostMapping("")
    public ResponseEntity<MedicineDto> createMedicine(@Valid @RequestBody MedicineDto newMedicine,
                                                      Authentication authentication) throws URISyntaxException {
        Medicine medicine = medicineService.createMedicine(authentication, newMedicine);
        MedicineDto dto = assembler.toModel(medicine);
        return ResponseEntity.created(new URI(dto.getRequiredLink("self").getHref())).build();
    }

    @Secured({"ROLE_ADMIN", "ROLE_DIRECTOR"})
    @PutMapping("/{id}")
    public ResponseEntity<MedicineDto> updateMedicine(@PathVariable Long id,
                                                      @Valid @RequestBody MedicineDto newMedicine) {
        Medicine medicine = medicineService.updateMedicine(id, newMedicine);
        MedicineDto dto = assembler.toModel(medicine);
        dto.setManagerId(medicine.getManager().getId().toString());
        return ResponseEntity.ok(dto);
    }

    @Secured({"ROLE_ADMIN", "ROLE_DIRECTOR"})
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMedicine(@PathVariable Long id) {
        medicineService.deleteMedicine(id);
        return ResponseEntity.ok("delete was successfully");
    }

    @GetMapping("/{id}")
    public ResponseEntity<MedicineDto> findById(@PathVariable Long id) {
        Medicine medicine = medicineService.findById(id);
        MedicineDto dto = assembler.toModel(medicine);
        dto.setManagerId(medicine.getManager().getId().toString());
        return ResponseEntity.ok(dto);
    }

}
