package ru.vironit.pharmacy.dto;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import javax.validation.constraints.*;

@Relation(collectionRelation = "customers")
@Data
public class CustomerDto extends RepresentationModel<CustomerDto> {

    private Long id;

    @Pattern(regexp = "^.+@.+\\..{2,}$")
    private String email;

    @NotNull
    @Size(min = 6)
    @Pattern(regexp = "^[a-zA-Z0-9]+$")
    private String password;

    @NotNull
    @Size(max = 50)
    @Pattern(regexp = "^[a-zA-Z]+$")
    private String name;

    @NotNull
    @Pattern(regexp = "^((\\+?375)([0-9]){9})$")
    private String phone;

    @Size(max = 100)
    @Pattern(regexp = "^[a-zA-Z0-9]+$")
    private String address;


}
