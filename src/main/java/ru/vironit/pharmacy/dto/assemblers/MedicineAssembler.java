package ru.vironit.pharmacy.dto.assemblers;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;
import ru.vironit.pharmacy.controllers.MedicineController;
import ru.vironit.pharmacy.dto.MedicineDto;
import ru.vironit.pharmacy.entities.Medicine;

@Component
public class MedicineAssembler extends RepresentationModelAssemblerSupport<Medicine, MedicineDto> {

    public MedicineAssembler() {
        super(MedicineController.class, MedicineDto.class);
    }

    public MedicineDto toModel(Medicine entity) {
        MedicineDto dto = createModelWithId(entity.getId(), entity);
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setPrescription(entity.getPrescription());
        dto.setProvider(entity.getProvider());
        dto.setPrice(entity.getPrice());
        return dto;
    }

    public void toEntity(Medicine medicine, MedicineDto medicineDTO){
        medicine.setName(medicineDTO.getName());
        medicine.setPrice(medicineDTO.getPrice());
        medicine.setDescription(medicineDTO.getDescription());
        medicine.setPrescription(medicineDTO.getPrescription());
        medicine.setProvider(medicineDTO.getProvider());
    }
}
