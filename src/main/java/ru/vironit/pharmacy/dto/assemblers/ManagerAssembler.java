package ru.vironit.pharmacy.dto.assemblers;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import ru.vironit.pharmacy.controllers.ManagerController;
import ru.vironit.pharmacy.dto.ManagerDto;
import ru.vironit.pharmacy.entities.Manager;
import ru.vironit.pharmacy.entities.Role;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@Component
public class ManagerAssembler extends RepresentationModelAssemblerSupport<Manager, ManagerDto> {

    public ManagerAssembler() {
        super(ManagerController.class, ManagerDto.class);
    }

    public ManagerDto toModel(Authentication authentication, Manager entity) {
        ManagerDto dto = instantiateModel(entity);
        setParams(dto, entity);
        dto.add(linkTo(methodOn(ManagerController.class).getManager(authentication)).withRel("update manager"));
        return dto;
    }

    public ManagerDto toModel(Manager entity) {
        ManagerDto dto = createModelWithId(entity.getId(), entity);
        setParams(dto, entity);
        return dto;
    }

    public void toEntity(Manager manager, ManagerDto dto){
        manager.setFirstName(dto.getFirstName());
        manager.setLastName(dto.getLastName());
        manager.setLogin(dto.getLogin());
        manager.setPassword(new BCryptPasswordEncoder().encode(dto.getPassword()));
        manager.setRole(Role.ROLE_ADMIN);
    }

    private void setParams(ManagerDto dto, Manager entity) {
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setLogin(entity.getLogin());
        dto.setPassword(entity.getPassword());
    }
}
