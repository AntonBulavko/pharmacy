package ru.vironit.pharmacy.dto.assemblers;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;
import ru.vironit.pharmacy.controllers.MedicineController;
import ru.vironit.pharmacy.controllers.OrderController;
import ru.vironit.pharmacy.dto.OrderItemDto;
import ru.vironit.pharmacy.entities.OrderItem;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@Component
public class OrderItemAssembler extends RepresentationModelAssemblerSupport<OrderItem, OrderItemDto> {

    public OrderItemAssembler() {
        super(OrderController.class, OrderItemDto.class);
    }

    @Override
    public OrderItemDto toModel(OrderItem entity) {
        OrderItemDto dto = instantiateModel(entity);
        dto.setMedicineId(entity.getMedicine().getId());
        dto.setQty(entity.getQty());
        dto.add(linkTo(methodOn(MedicineController.class).findById(entity.getMedicine().getId())).withRel("medicine"));
        return dto;
    }

}
