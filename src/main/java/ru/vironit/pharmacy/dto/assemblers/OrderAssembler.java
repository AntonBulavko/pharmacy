package ru.vironit.pharmacy.dto.assemblers;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import ru.vironit.pharmacy.controllers.ManagerController;
import ru.vironit.pharmacy.controllers.OrderController;
import ru.vironit.pharmacy.controllers.PersonalProfileController;
import ru.vironit.pharmacy.dto.OrderDto;
import ru.vironit.pharmacy.dto.OrderItemDto;
import ru.vironit.pharmacy.entities.Order;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@Component
public class OrderAssembler extends RepresentationModelAssemblerSupport<Order, OrderDto> {

    public OrderAssembler() {
        super(OrderController.class, OrderDto.class);
    }

    @Override
    public OrderDto toModel(Order entity) {
        OrderDto dto = createModelWithId(entity.getOrderId(), entity);
        setParams(dto, entity);
        CollectionModel<OrderItemDto> items = new OrderItemAssembler().toCollectionModel(entity.getItems());
        dto.setItems(items);
        if (entity.getManager() != null) {
            dto.setManagerId(entity.getManager().getId().toString());
            dto.add(linkTo(methodOn(ManagerController.class).getManager(Long.valueOf(dto.getManagerId()))).withRel("manager"));
        }
        return dto;
    }

    public OrderDto toModel(Authentication authentication, Order entity) {
        OrderDto dto = instantiateModel(entity);
        setParams(dto, entity);
        CollectionModel<OrderItemDto> items = new OrderItemAssembler().toCollectionModel(entity.getItems());
        dto.setItems(items);
        dto.add(linkTo(methodOn(PersonalProfileController.class).getCustomer(authentication)).withRel("customer info"));
        return dto;
    }

    private void setParams(OrderDto dto, Order entity) {
        dto.setUser(entity.getCustomer().getPhone());
        dto.setOrderId(entity.getOrderId());
        dto.setDateCreated(entity.getDateCreated().toString());
        if (entity.getDateCompleted() == null) {
            dto.setDateCompleted("this order still in progress");
        } else {
            dto.setDateCompleted(entity.getDateCompleted().toString());
        }
        dto.setTotalCost(entity.getTotalCost());
        dto.setStatus(entity.getStatus().toString());
    }
}
