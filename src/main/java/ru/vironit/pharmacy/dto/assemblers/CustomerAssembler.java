package ru.vironit.pharmacy.dto.assemblers;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import ru.vironit.pharmacy.controllers.CustomerController;
import ru.vironit.pharmacy.controllers.PersonalProfileController;
import ru.vironit.pharmacy.dto.CustomerDto;
import ru.vironit.pharmacy.entities.Customer;
import ru.vironit.pharmacy.entities.Role;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@Component
public class CustomerAssembler extends RepresentationModelAssemblerSupport<Customer, CustomerDto> {

    public CustomerAssembler() {
        super(CustomerController.class, CustomerDto.class);
    }

    @Override
    public CustomerDto toModel(Customer entity) {
        CustomerDto dto = createModelWithId(entity.getId(), entity);
        dto.setId(entity.getId());
        setParams(dto, entity);
        dto.add(linkTo(methodOn(CustomerController.class).getAllByCustomer(entity.getId())).withRel("customer orders"));
        return dto;
    }

    public CustomerDto toModel(Authentication authentication, Customer entity) {
        CustomerDto dto = instantiateModel(entity);
        setParams(dto, entity);
        dto.add(linkTo(methodOn(PersonalProfileController.class).getCustomer(authentication)).withRel("customer info"),
                linkTo(methodOn(PersonalProfileController.class).customerOrders(null, null, authentication)).withRel("customer orders"));
        return dto;
    }

    public void toEntity(Customer customer, CustomerDto customerDTO) {
        customer.setName(customerDTO.getName());
        customer.setEmail(customerDTO.getEmail());
        customer.setAddress(customerDTO.getAddress());
        customer.setPassword(new BCryptPasswordEncoder().encode(customerDTO.getPassword()));
        customer.setRole(Role.ROLE_USER);
        customer.setPhone(customerDTO.getPhone());
    }

    private void setParams(CustomerDto dto, Customer entity) {
        dto.setName(entity.getName());
        dto.setAddress(entity.getAddress());
        dto.setEmail(entity.getEmail());
        dto.setPhone(entity.getPhone());
    }

}
