package ru.vironit.pharmacy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@Relation(collectionRelation = "items in order")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemDto extends RepresentationModel<OrderItemDto> {

    private Long medicineId;
    private Integer qty;

}
