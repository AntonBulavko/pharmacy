package ru.vironit.pharmacy.dto;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import javax.validation.constraints.NotNull;

@Relation(collectionRelation = "medicines")
@Data
public class MedicineDto extends RepresentationModel<MedicineDto> {

    @NotNull
    private String name;

    @NotNull
    private float price;

    @NotNull
    private String description;

    @NotNull
    private Boolean prescription;

    private String managerId;

    @NotNull
    private String provider;

}
