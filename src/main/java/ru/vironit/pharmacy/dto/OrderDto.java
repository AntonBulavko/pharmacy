package ru.vironit.pharmacy.dto;

import lombok.Data;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@Relation(collectionRelation = "orders")
@Data
public class OrderDto extends RepresentationModel<OrderDto> {

    private String user;

    private long orderId;

    private String dateCreated;

    private String dateCompleted;

    private double totalCost;

    private String status;

    private String managerId;

    private CollectionModel<OrderItemDto> items;

}
