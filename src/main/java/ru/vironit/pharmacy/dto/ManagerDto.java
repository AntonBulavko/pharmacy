package ru.vironit.pharmacy.dto;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Relation(collectionRelation = "managers")
@Data
public class ManagerDto extends RepresentationModel<ManagerDto> {

    @NotNull
    @Size(max = 50)
    @Pattern(regexp = "^[a-zA-Z]+$")
    private String firstName;

    @NotNull
    @Size(max = 50)
    @Pattern(regexp = "^[a-zA-Z]+$")
    private String lastName;

    @NotNull
    @Size(max = 50)
    @Pattern(regexp = "^[a-zA-Z0-9]+$")
    private String login;

    @NotNull
    @Size(min = 6)
    @Pattern(regexp = "^[a-zA-Z0-9]+$")
    private String password;

    private String employmentDate;

}
