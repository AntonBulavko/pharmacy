package ru.vironit.pharmacy.dto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class AuthorizationDto {

    private String login;

    @NotNull
    @Size(min = 6)
    @Pattern(regexp = "^[a-zA-Z0-9]+$")
    private String password;

}
