package ru.vironit.pharmacy.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vironit.pharmacy.dto.OrderItemDto;
import ru.vironit.pharmacy.entities.*;
import ru.vironit.pharmacy.exception.*;
import ru.vironit.pharmacy.repos.OrderRepo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;

@Transactional
@Service
@Slf4j
public class OrderService {

    private final OrderRepo orderRepo;
    private final OrderItemService itemService;
    private final MedicineService medicineService;
    private final ManagerService managerService;
    private final CustomerService customerService;

    public OrderService(OrderRepo orderRepo,
                        OrderItemService itemService, MedicineService medicineService, ManagerService managerService, CustomerService customerService) {
        this.orderRepo = orderRepo;
        this.itemService = itemService;
        this.medicineService = medicineService;
        this.managerService = managerService;
        this.customerService = customerService;
    }

    public Page<Order> findAll(String managerLogin, String user, Status status, String date, Pageable pageable) throws ParseException {
        Manager manager = null;
        Customer customer = null;
        ExampleMatcher matcher = ExampleMatcher.matchingAll()
                .withMatcher("dateCreated", contains());
        if (managerLogin != null) {
            manager = managerService.findByLogin(managerLogin);
        }
        if (user != null) {
            customer = customerService.findCustomerByPhone(user);
        }

        Order order = new Order();
        order.setManager(manager);
        order.setCustomer(customer);
        if (date != null){

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date docDate = formatter.parse(date);
            java.sql.Date sqlDate = new java.sql.Date(docDate.getTime());
            /*formatter.*/
            /*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate docDate = LocalDate.parse(date,formatter);*/
            order.setDateCreated(sqlDate);
        }
        order.setStatus(status);
        return orderRepo.findAll(Example.of(order,matcher), pageable);
    }

    public Order findById(Long orderId) {
        return orderRepo.findById(orderId).orElseThrow(() -> new OrderException("no order with this id:" + orderId));
    }

    public Page<Order> findByCustomer(Long customerId, Pageable pageable) {
        Page<Order> orders = orderRepo.findByCustomer_Id(customerId, pageable).orElse(null);
        if (orders != null) {
            for (Order order : orders) {
                order.setItems(itemService.findItemsByOrder(order.getOrderId()));
            }
        }
        return orders;
    }

    public List<Order> findByManager(Long managerId) {
        return orderRepo.findByManager_Id(managerId).orElse(null);
    }

    public Order createNewOrder(Customer customer, List<OrderItemDto> items) {
        Order order = new Order();
        order.setCustomer(customer);
        order.setStatus(Status.IN_PROGRESS);
        fillOrder(order, items);
        order.setDateCreated(new java.sql.Date(System.currentTimeMillis()));
        recalculateOrder(order);
        orderRepo.save(order);
        return order;
    }

    public Order cancelOrder(Manager manager, Long orderId) {
        Order order = findById(orderId);
        if (order.getStatus() == Status.IN_PROGRESS) {
            order.setStatus(Status.CANCELED);
            order.setManager(manager);
            return orderRepo.save(order);
        }
        throw new OrderException("can't change status, when order status:" + order.getStatus());
    }

    private void recalculateOrder(Order order) {
        double totalCost = 0;
        for (OrderItem item : order.getItems()) {
            totalCost += item.getQty() * item.getMedicine().getPrice();
        }
        order.setTotalCost(totalCost);
    }

    public Order completeOrder(Manager manager, Long orderId) {
        Order order = findById(orderId);
        if (order.getStatus() == Status.IN_PROGRESS) {
            order.setStatus(Status.COMPLETED);
            order.setDateCompleted(new java.sql.Date(System.currentTimeMillis()));
            order.setManager(manager);
            return orderRepo.save(order);
        }
        throw new OrderException("can't change status, when order status:" + order.getStatus());
    }

    private void fillOrder(Order order, List<OrderItemDto> items) {
        for (OrderItemDto dto : items) {
            Medicine medicine = medicineService.findById(dto.getMedicineId());
            int qty = dto.getQty();
            OrderItem item = new OrderItem();
            item.setOrder(order);
            item.setMedicine(medicine);
            item.setQty(qty);
            order.getItems().add(item);
        }
    }

}
