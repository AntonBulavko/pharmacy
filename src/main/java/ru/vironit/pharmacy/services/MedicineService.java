package ru.vironit.pharmacy.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vironit.pharmacy.dto.MedicineDto;
import ru.vironit.pharmacy.dto.assemblers.MedicineAssembler;
import ru.vironit.pharmacy.entities.Manager;
import ru.vironit.pharmacy.entities.Medicine;
import ru.vironit.pharmacy.exception.*;
import ru.vironit.pharmacy.repos.MedicineRepo;
import ru.vironit.pharmacy.security.JwtUser;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;

@Service
@Transactional
@Slf4j
public class MedicineService {

    private final MedicineRepo medicineRepo;
    private final ManagerService managerService;
    private final MedicineAssembler assembler;

    public MedicineService(MedicineRepo medicineRepo,
                           ManagerService managerService,
                           MedicineAssembler assembler) {
        this.medicineRepo = medicineRepo;
        this.managerService = managerService;
        this.assembler = assembler;
    }

    public Medicine findById(Long id) {
        return medicineRepo.findById(id).orElseThrow(() -> new MedicineException("no medicine with this id: " + id));
    }

    public Medicine findByName(String name) {
        return medicineRepo.findByName(name).orElseThrow(() -> new MedicineException("no medicine with this name: " + name));
    }

    public Page<Medicine> findAll(String name, String provider, String description, Pageable pageable) {
        ExampleMatcher matcher = ExampleMatcher.matchingAll()
                .withMatcher("name", contains().ignoreCase())
                .withMatcher("provider", contains().ignoreCase())
                .withMatcher("description", contains().ignoreCase());
        Medicine medicine = new Medicine();
        medicine.setName(name);
        medicine.setProvider(provider);
        medicine.setDescription(description);
        return medicineRepo.findAll(Example.of(medicine, matcher), pageable);
    }

    public Medicine createMedicine(Authentication authentication, MedicineDto medicineDTO) {
        JwtUser user = (JwtUser) authentication.getPrincipal();
        Manager manager = managerService.findByLogin(user.getUsername());
        Medicine medicine = new Medicine();
        medicine.setManager(manager);
        assembler.toEntity(medicine, medicineDTO);
        return saveMedicine(medicine);
    }

    public Medicine updateMedicine(Long id, MedicineDto newMedicine) {
        if (existsByNameAndProvider(newMedicine.getName(), newMedicine.getProvider())) {
            if (findByName(newMedicine.getName()).getId().equals(id)) {
                return saveUpdatedMedicine(id, newMedicine);
            }
            throw new MedicineException("medicine exist with name" + newMedicine.getName());
        }
        return saveUpdatedMedicine(id, newMedicine);
    }

    public void deleteMedicine(Long id) {
        medicineRepo.delete(findById(id));
    }

    private boolean existsByNameAndProvider(String name, String provider) {
        return medicineRepo.existsByNameAndProvider(name, provider);
    }

    private Medicine saveUpdatedMedicine(Long id, MedicineDto newMedicine) {
        return medicineRepo.findById(id).map(medicine -> {
            assembler.toEntity(medicine, newMedicine);
            return medicineRepo.save(medicine);
        }).orElseThrow(() -> new MedicineException("no medicine with this id" + id));
    }

    private Medicine saveMedicine(Medicine medicine) {
        if (existsByNameAndProvider(medicine.getName(), medicine.getProvider())) {
            throw new MedicineException("medicine exist with name " + medicine.getName() + ", and from provider: " + medicine.getProvider());
        }
        return medicineRepo.save(medicine);
    }
}
