package ru.vironit.pharmacy.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import ru.vironit.pharmacy.dto.ManagerDto;
import ru.vironit.pharmacy.dto.assemblers.ManagerAssembler;
import ru.vironit.pharmacy.entities.*;
import ru.vironit.pharmacy.exception.*;
import ru.vironit.pharmacy.repos.ManagerRepo;

import javax.transaction.Transactional;
import java.util.Date;

@Service
@Transactional
@Slf4j
public class ManagerService {

    private final ManagerRepo managerRepo;
    private final ManagerAssembler assembler;

    public ManagerService(ManagerRepo managerRepo, ManagerAssembler assembler) {
        this.managerRepo = managerRepo;
        this.assembler = assembler;
    }

    public Page<Manager> findAll(String orderId, Pageable pageable) {
        Order order = new Order();
        order.setOrderId(Long.valueOf(orderId));
        Manager manager = new Manager();
        manager.getOrdersCompleted().add(order);
        return managerRepo.findAll(Example.of(manager), pageable);
    }

    public Manager findById(Long id) {
        return managerRepo.findById(id).orElseThrow(() -> new ManagerException("no manager with this id:" + id));
    }

    public Manager updateManager(Long id, ManagerDto newManager) {
        if (existByLogin(newManager.getLogin())) {
            if (findByLogin(newManager.getLogin()).getId().equals(id)) {
                return saveUpdatedManager(id, newManager);
            }
            throw new ManagerException("this login is busy " + newManager.getLogin());
        }
        return saveUpdatedManager(id, newManager);
    }

    public Manager createManager(ManagerDto managerDTO) {
        Manager manager = new Manager();
        assembler.toEntity(manager, managerDTO);
        manager.setEmploymentDate(new Date());
        return saveManager(manager);
    }

    public void deleteManager(Long id) {
        managerRepo.delete(findById(id));
    }

    public Manager findByLogin(String login) {
        return managerRepo.findByLogin(login).orElseThrow(() -> new ManagerException("no manager with this login: " + login));
    }

    private Manager saveManager(Manager manager) {
        if (existByLogin(manager.getLogin())) {
            throw new ManagerException("this login is busy " + manager.getLogin());
        }
        return managerRepo.save(manager);
    }

    private Manager saveUpdatedManager(Long id, ManagerDto newManager) {
        return managerRepo.findById(id).map(manager -> {
            assembler.toEntity(manager, newManager);
            return managerRepo.save(manager);
        }).orElseThrow(() -> new ManagerException("no manager with this id: " + id));
    }

    private boolean existByLogin(String login) {
        return managerRepo.existsByLogin(login);
    }

}
