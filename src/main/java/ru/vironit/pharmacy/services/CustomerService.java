package ru.vironit.pharmacy.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vironit.pharmacy.dto.CustomerDto;
import ru.vironit.pharmacy.dto.assemblers.CustomerAssembler;
import ru.vironit.pharmacy.entities.*;
import ru.vironit.pharmacy.exception.CustomerException;
import ru.vironit.pharmacy.repos.CustomerRepo;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;

@Service
@Transactional
@Slf4j
public class CustomerService {

    private final CustomerRepo customerRepo;
    private final CustomerAssembler assembler;

    public CustomerService(CustomerRepo customerRepo, CustomerAssembler assembler) {
        this.customerRepo = customerRepo;
        this.assembler = assembler;
    }

    public Page<Customer> findAll(Pageable pageable) {
        return customerRepo.findAll(pageable);
    }

    public Customer findCustomerById(Long customerId) {
        return customerRepo.findById(customerId).orElseThrow(() -> new CustomerException("no customer with id: " + customerId));
    }

    public Customer createCustomer(CustomerDto customerDTO) {
        Customer customer = new Customer();
        assembler.toEntity(customer, customerDTO);
        return saveCustomer(customer);
    }

    public Customer updateCustomer(Long id, CustomerDto newCustomer) {
        if (existByPhone(newCustomer.getPhone())) {
            if (id.equals(findCustomerByPhone(newCustomer.getPhone()).getId())) {
                return saveUpdatedCustomer(id, newCustomer);
            }
            throw new CustomerException("this phone: " + newCustomer.getPhone() + " is busy");
        }
        return saveUpdatedCustomer(id, newCustomer);
    }

    public void deleteCustomer(Long customerId) {
        customerRepo.delete(findCustomerById(customerId));
    }

    public Customer findCustomerByPhone(String phone) {
        return customerRepo.findByPhone(phone).orElseThrow(() -> new CustomerException("no customer with this phone"));
    }

    private Customer saveUpdatedCustomer(Long id, CustomerDto newCustomer) {
        return customerRepo.findById(id).map(customer -> {
            assembler.toEntity(customer,newCustomer);
            return customerRepo.save(customer);
        }).orElseThrow(() -> new CustomerException("no customer with id: " + id));
    }

    private boolean existByPhone(String phone) {
        return customerRepo.existsByPhone(phone);
    }

    private Customer saveCustomer(Customer customer) {
        if (existByPhone(customer.getPhone())) {
            throw new CustomerException("this phone: " + customer.getPhone() + " is busy");
        }
        return customerRepo.save(customer);
    }
}