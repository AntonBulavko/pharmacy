package ru.vironit.pharmacy.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vironit.pharmacy.entities.Medicine;
import ru.vironit.pharmacy.entities.OrderItem;
import ru.vironit.pharmacy.exception.OrderException;
import ru.vironit.pharmacy.repos.OrderItemRepo;

import java.util.List;

@Service
@Transactional
@Slf4j
public class OrderItemService {

    private final OrderItemRepo orderItemRepo;

    public OrderItemService(OrderItemRepo orderItemRepo) {
        this.orderItemRepo = orderItemRepo;
    }

    public List<OrderItem> findItemsByOrder(Long orderId) {
        return orderItemRepo.findByOrder_OrderId(orderId).orElseThrow(() -> new OrderException("no order with id:" + orderId));
    }

}
