package ru.vironit.pharmacy.security;

import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.Service;
import ru.vironit.pharmacy.entities.*;
import ru.vironit.pharmacy.services.*;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    private final CustomerService customerService;
    private final ManagerService managerService;

    public JwtUserDetailsService(CustomerService customerService, ManagerService managerService) {
        this.customerService = customerService;
        this.managerService = managerService;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        try {
            Customer customer = customerService.findCustomerByPhone(login);
            return JwtUserFactory.createCustomer(customer);
        } catch (Exception e){
            Manager manager = managerService.findByLogin(login);
            return JwtUserFactory.createManager(manager);
        }
    }
}
