package ru.vironit.pharmacy.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import ru.vironit.pharmacy.entities.*;

public class JwtUserFactory {

    public JwtUserFactory() {
    }

    public static JwtUser createCustomer(Customer customer) {
        return new JwtUser(customer.getId(),
                customer.getPhone(),
                customer.getPassword(),
                toGrantedAuthorities(customer.getRole())
        );
    }

    public static JwtUser createManager(Manager manager) {
        return new JwtUser(manager.getId(),
                manager.getLogin(),
                manager.getPassword(),
                toGrantedAuthorities(manager.getRole()));
    }

    private static GrantedAuthority toGrantedAuthorities(Role role) {
        return new SimpleGrantedAuthority(role.toString());
    }
}
