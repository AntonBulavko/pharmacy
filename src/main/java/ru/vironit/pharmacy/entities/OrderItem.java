package ru.vironit.pharmacy.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"order", "medicine"})
@Table(name = "order_items")
public class OrderItem implements Serializable {

    @EmbeddedId
    private OrderItemPK pk = new OrderItemPK();

    @MapsId("orderId")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", referencedColumnName = "id")
    private Order order;

    @MapsId("medicineId")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "med_id", referencedColumnName = "id")
    private Medicine medicine;

    @Column(name = "qty")
    private Integer qty;

    public void setOrder(Order order) {
        this.order = order;
        getPk().setOrderId(order.getOrderId());
    }

    public void setMedicine(Medicine medicine) {
        this.medicine = medicine;
        getPk().setMedicineId(medicine.getId());
    }

}
