package ru.vironit.pharmacy.entities;

public enum Status {
    IN_PROGRESS,
    COMPLETED,
    CANCELED
}

