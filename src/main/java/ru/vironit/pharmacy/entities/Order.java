package ru.vironit.pharmacy.entities;

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"items", "manager"})
@Table(name = "orders")
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long orderId;

    @ManyToOne
    @JoinColumn(name = "cust_id", referencedColumnName = "id")
    private Customer customer;

    @Column(name = "total_cost")
    private Double totalCost;

    @Column(name = "date_created")
    private Date dateCreated;

    @Column(name = "date_completed")
    private Date dateCompleted;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private Collection<OrderItem> items = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "manager_id", referencedColumnName = "id")
    private Manager manager;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(orderId, order.orderId) &&
                Objects.equals(customer, order.customer) &&
                Objects.equals(totalCost, order.totalCost) &&
                Objects.equals(dateCreated, order.dateCreated) &&
                Objects.equals(dateCompleted, order.dateCompleted) &&
                status == order.status &&
                Objects.equals(items, order.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, customer, totalCost, dateCreated, dateCompleted, status, items);
    }

}
