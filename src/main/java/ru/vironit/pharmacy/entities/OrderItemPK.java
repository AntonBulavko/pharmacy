package ru.vironit.pharmacy.entities;

import lombok.*;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderItemPK implements Serializable {

    private Long orderId;
    private Long medicineId;

}
