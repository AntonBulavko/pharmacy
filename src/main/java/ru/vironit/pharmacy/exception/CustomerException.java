package ru.vironit.pharmacy.exception;

public class CustomerException extends RuntimeException {
   
    public CustomerException(String message) {
        super(message);
    }

}
