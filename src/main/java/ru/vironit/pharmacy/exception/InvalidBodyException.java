package ru.vironit.pharmacy.exception;

public class InvalidBodyException extends RuntimeException {

    public InvalidBodyException() {
        super("field's cant be empty");
    }
}
