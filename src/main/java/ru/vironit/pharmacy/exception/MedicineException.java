package ru.vironit.pharmacy.exception;

public class MedicineException extends RuntimeException {
    
    public MedicineException(String message) {
        super(message);
    }

}
