package ru.vironit.pharmacy.exception;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.*;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import ru.vironit.pharmacy.security.JwtAuthenticationException;
import javax.persistence.PersistenceException;
import java.util.*;

@ControllerAdvice
public class ExceptionHandler {

    private final MessageSource messageSource;

    public ExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ResponseBody
    @org.springframework.web.bind.annotation.ExceptionHandler({
            CustomerException.class, ManagerException.class,
            MedicineException.class, InvalidBodyException.class, OrderException.class, JwtAuthenticationException.class
    })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String exceptionHandler(RuntimeException ex) {
        return ex.getMessage();
    }

    @ResponseBody
    @org.springframework.web.bind.annotation.ExceptionHandler({
            NullPointerException.class
    })
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    String serverError(PersistenceException ex) {
        return ex.getLocalizedMessage();
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ResponseBody
    public ValidationError getValidationError(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        return processFieldErrors(fieldErrors);
    }

    private ValidationError processFieldErrors(List<FieldError> fieldErrors) {
        ValidationError error = new ValidationError();
        for (FieldError fieldError : fieldErrors) {
            String message = resolveErrorMessage(fieldError);
            error.addFieldError(fieldError.getField(), message);
        }
        return error;
    }

    private String resolveErrorMessage(FieldError fieldError) {
        Locale currentLocale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(fieldError, currentLocale);
    }
}
