package ru.vironit.pharmacy.exception;

public class ManagerException extends RuntimeException {

    public ManagerException(String message) {
        super(message);
    }
}
