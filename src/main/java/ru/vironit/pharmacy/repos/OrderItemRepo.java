package ru.vironit.pharmacy.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vironit.pharmacy.entities.OrderItem;
import java.util.*;

@Repository
public interface OrderItemRepo extends JpaRepository<OrderItem, Long> {

   Optional<List<OrderItem>> findByOrder_OrderId(Long orderId);

}
