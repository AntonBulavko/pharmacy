package ru.vironit.pharmacy.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vironit.pharmacy.entities.Manager;
import ru.vironit.pharmacy.entities.Order;

import java.util.Optional;

@Repository
public interface ManagerRepo extends JpaRepository<Manager, Long> {

    Optional<Manager> findByLogin(String login);

    Manager findByOrdersCompleted(Order order);

    boolean existsByLogin(String login);

}
