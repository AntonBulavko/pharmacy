package ru.vironit.pharmacy.repos;

import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vironit.pharmacy.entities.Order;
import java.util.*;

@Repository
public interface OrderRepo extends JpaRepository<Order, Long> {

    Optional<Page<Order>> findByCustomer_Id(Long customerId, Pageable pageable);

    Optional<ArrayList<Order>> findByManager_Id(Long managerId);

}
