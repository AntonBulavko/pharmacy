package ru.vironit.pharmacy.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vironit.pharmacy.entities.Customer;

import java.util.ArrayList;
import java.util.Optional;

@Repository

public interface CustomerRepo extends JpaRepository<Customer, Long> {

    Optional<Customer> findByPhone(String phone);

    boolean existsByPhone(String phone);
}
