package ru.vironit.pharmacy.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vironit.pharmacy.entities.Medicine;
import java.util.Optional;

@Repository
public interface MedicineRepo extends JpaRepository<Medicine, Long> {

    Optional<Medicine> findByName(String name);

    boolean existsByNameAndProvider(String name, String provider);

}