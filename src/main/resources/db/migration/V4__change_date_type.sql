
alter table order_items drop constraint order_items_medicines_fk;

alter table order_items
	add constraint order_items_medicines_fk
		foreign key (med_id) references medicines
			on update cascade on delete cascade;

alter table order_items drop constraint order_items_orders_fk;

alter table order_items
	add constraint order_items_orders_fk
		foreign key (order_id) references orders
			on update cascade on delete cascade;