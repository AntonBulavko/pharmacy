CREATE SEQUENCE hibernate_sequence START 1 INCREMENT 1;

create table customers
(
    id       bigserial           not null,
    address  varchar(255),
    email    varchar(255),
    name     varchar(255),
    password varchar(255)        not null,
    phone    varchar(255) unique not null,
    role     varchar(255),
    primary key (id)
);

create table managers
(
    id              bigserial           not null,
    employment_date timestamp,
    first_name      varchar(255)        not null,
    last_name       varchar(255)        not null,
    login           varchar(255) unique not null,
    password        varchar(255)        not null,
    role            varchar(255)        not null,
    primary key (id)
);

create table medicines
(
    id           bigserial           not null,
    description  varchar(255)        not null,
    name         varchar(255) unique not null,
    prescription boolean             not null,
    provider     varchar(255)       not null,
    price        float4              not null,
    manager_id   int8                not null,
    primary key (id)
);

create table order_items
(
    med_id   int8 not null,
    order_id int8 not null,
    qty      int4 not null,
    primary key (med_id, order_id)
);

create table orders
(
    id             bigserial    not null,
    date_completed timestamp,
    date_created   timestamp not null,
    status         varchar(255) not null,
    total_cost     float8       not null,
    cust_id        int8         not null,
    manager_id     int8,
    primary key (id)
);

alter table order_items
    add constraint order_items_medicines_fk
        foreign key (med_id) references medicines
            on delete set null on update cascade;

alter table order_items
    add constraint order_items_orders_fk
        foreign key (order_id) references orders
            on delete set null on update cascade;

alter table orders
    add constraint orders_customers_fk
        foreign key (cust_id) references customers
            on delete set null on update cascade;

alter table orders
    add constraint orders_managers_fk
        foreign key (manager_id) references managers
            on delete set null on update cascade;

alter table medicines
    add constraint medicines_managers_id_fk
        foreign key (manager_id) references managers
            on update cascade on delete cascade;

