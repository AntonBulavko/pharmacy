alter table medicines drop constraint medicines_name_key;

alter table medicines
	add constraint medicines_name_key
		unique (name, provider);