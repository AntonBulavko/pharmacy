alter table medicines drop constraint medicines_managers_id_fk;

alter table medicines
	add constraint medicines_managers_id_fk
		foreign key (manager_id) references managers
			on update cascade on delete set null;
